import dto.Serie;
import dto.Videojuego;

public class EjecutableApp {

	public static void main(String[] args) {
		//Creamos array de 5 posiciones de los objetos
		Serie arraySeries[] = new Serie[5];
		Videojuego arrayVideojuegos[] = new Videojuego[5];
		
		//Rellenamos los arrays con objetos
		arraySeries[0]=new Serie("The Mandalorian", "Bryce Dallas Howard");
		arraySeries[1]=new Serie("La Casa de Papel", 4, "Robo", "Guillermo del Toro");
		arraySeries[2]=new Serie("Big Mouth", "Quentin Tarantino");
		arraySeries[3]=new Serie();
		arraySeries[4]=new Serie("The Witcher", 7, "Mediaval", "Tomasz Baginski");
  
		arrayVideojuegos[0]=new Videojuego("Super Mario Bros", 85);
		arrayVideojuegos[1]=new Videojuego("Undertale", 70, "Indie", "Toby Fox");
		arrayVideojuegos[2]=new Videojuego("The Witcher 3", 120);
		arrayVideojuegos[3]=new Videojuego("Dark Souls", 135, "RPG", "From Software");
		arrayVideojuegos[4]=new Videojuego();
		
		//Llamamos al metodo entregar con 2 series y 2 videojuegos
		arraySeries[1].entregar();
		arraySeries[3].entregar();
		arrayVideojuegos[2].entregar();
		arrayVideojuegos[4].entregar();
		
		int entregados = 0;
		
		//Contamos cuantas series y videojuegos hay entregados y los devolvemos llamando al metodo devolver
		for (int cont = 0; cont < arrayVideojuegos.length; cont++) {
			//Series
			if (arraySeries[cont].isEntregado()) {
				entregados++;
				arraySeries[cont].devolver();
			}

			//Videojuegos
			if (arrayVideojuegos[cont].isEntregado()) {
				entregados++;
				arrayVideojuegos[cont].devolver();
			}
		}
		
		Serie serieMayor = arraySeries[0];
		Videojuego videojuegoMayor = arrayVideojuegos[0];
		
		//Comparamos los objetos del array de series y videojuegos y guardamos el que tiene mas temporadas o horas estimadas
		for (int cont = 1; cont < arrayVideojuegos.length; cont++) {
			if (arraySeries[cont].compareTo(serieMayor) == "mas") {
				serieMayor = arraySeries[cont];
			}
			
			if (arrayVideojuegos[cont].compareTo(videojuegoMayor) == "mas") {
				videojuegoMayor = arrayVideojuegos[cont];
			}
		}
		
		//Mostramos por consola toda la informacion que se nos pide
		System.out.println("Series y videojuegos entregados: " + entregados);
		System.out.println("");
		System.out.println("Serie con mas temporadas: ");
		System.out.println(serieMayor);
		System.out.println("");
		System.out.println("Videojuego con mas horas estimadas: ");
		System.out.println(videojuegoMayor);
	}
}
