package dto;
import services.IEntregable;

public class Videojuego implements IEntregable{

	//Atributos
	protected String titulo;
	protected double horasEstimadas;
	protected boolean entregado;
	protected String genero;
	protected String compania;
	
	//Constantes
	final String TITULO = "";
	final double HORASESTIMADAS = 10;
	final boolean ENTREGADO = false;
	final String GENERO = "";
	final String COMPANIA = "";
	
	//Constructor por defecto
	public Videojuego() {
		this.titulo=TITULO;
		this.horasEstimadas=HORASESTIMADAS;
		this.entregado=ENTREGADO;
		this.genero=GENERO;
		this.compania=COMPANIA;
	}

	//Constructor con el titulo y horas estimadas. El resto por defecto
	public Videojuego(String titulo, double horasEstimadas) {
		this.titulo=titulo;
		this.horasEstimadas=horasEstimadas;
		this.entregado=ENTREGADO;
		this.genero=GENERO;
		this.compania=COMPANIA;
	}
	
	//Constructor con todos los atributos excepto entregado
	public Videojuego(String titulo, double horasEstimadas, String genero, String compania) {
		this.titulo=titulo;
		this.horasEstimadas=horasEstimadas;
		this.entregado=ENTREGADO;
		this.genero=genero;
		this.compania=compania;
	}

	//Metodos
	public void entregar() {
        entregado = true;
    }
	
	public void devolver() {
        entregado = false;
    }
	
	public boolean isEntregado() {
        if(entregado){
            return true;
        }
        
        return false;
    }
	
	public String compareTo(Object a) {
		Videojuego aux = (Videojuego)a;
		
		if (horasEstimadas > aux.getHorasEstimadas()) {
			return "mas";
		} else {
			return "";
		}
	}
	
	//Metodo toString
	public String toString() {
		return "Titulo: " + titulo + ", Horas estimadas: " + horasEstimadas + ", Entregado: " + entregado
				+ ", Genero: " + genero + ", Compania: " + compania;
	}

	//Getters y Setters
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getHorasEstimadas() {
		return horasEstimadas;
	}

	public void setHorasEstimadas(int horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}
}
