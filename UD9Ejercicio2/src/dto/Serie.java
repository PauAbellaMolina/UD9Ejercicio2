package dto;
import services.IEntregable;

public class Serie implements IEntregable {

	//Atributos
	protected String titulo;
	protected int numeroTemporadas;
	protected boolean entregado;
	protected String genero;
	protected String creador;
	
	//Constantes
	final String TITULO = "";
	final int NUMEROTEMPORADAS = 3;
	final boolean ENTREGADO = false;
	final String GENERO = "";
	final String CREADOR = "";

	//Constructor por defecto
	public Serie() {
		this.titulo=TITULO;
		this.numeroTemporadas=NUMEROTEMPORADAS;
		this.entregado=ENTREGADO;
		this.genero=GENERO;
		this.creador=CREADOR;
	}

	//Constructor con el titulo y creador. El resto por defecto
	public Serie(String titulo, String creador) {
		this.titulo=titulo;
		this.numeroTemporadas=NUMEROTEMPORADAS;
		this.entregado=ENTREGADO;
		this.genero=GENERO;
		this.creador=creador;
	}
	
	//Constructor con todos los atributos excepto entregado
	public Serie(String titulo, int numeroTemporadas, String genero, String creador) {
		this.titulo=titulo;
		this.numeroTemporadas=numeroTemporadas;
		this.entregado=ENTREGADO;
		this.genero=genero;
		this.creador=creador;
	}

	//Metodos
	public void entregar() {
        entregado = true;
    }
	
	public void devolver() {
        entregado = false;
    }
	
	public boolean isEntregado() {
        if(entregado){
            return true;
        }
        
        return false;
    }

	public String compareTo(Object a) {
		Serie aux = (Serie)a;
		
		if (numeroTemporadas > aux.getNumeroTemporadas()) {
			return "mas";
		} else {
			return "";
		}
	}

	//Metodo toString
	public String toString() {
		return "Titulo: " + titulo + ", Num. Temporadas: " + numeroTemporadas + ", Serie entregada: " + entregado
				+ ", Genero: " + genero + ", Creador: " + creador;
	}

	//Getters y Setters
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getNumeroTemporadas() {
		return numeroTemporadas;
	}

	public void setNumeroTemporadas(int numeroTemporadas) {
		this.numeroTemporadas = numeroTemporadas;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}
}
