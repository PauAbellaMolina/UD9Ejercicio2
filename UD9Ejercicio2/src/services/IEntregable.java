package services;

public interface IEntregable {
    public void entregar();
    
    public void devolver();
  
    public boolean isEntregado();
  
    public String compareTo(Object a);
}
